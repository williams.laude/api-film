-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        wil
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-24 16:13
-- Created:       2022-02-24 16:13
PRAGMA foreign_keys = OFF;

-- Schema: mydb
-- ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "Films"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "titre" VARCHAR(45) NOT NULL,
  "directors" VARCHAR(45) NOT NULL,
  "years" INTEGER NOT NULL,
  "img_url" VARCHAR(45)
);
COMMIT;
