import { Application } from "express";
import HomeController from "./controllers/HomeController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/films', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/films/:id',(req,res) => {
        HomeController.filmsdetails(req, res)
    })

}
