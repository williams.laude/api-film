import { Request, Response } from "express-serve-static-core";
import {db} from '../server';


export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        const films = db.prepare('SELECT * FROM Films').all()
        console.log(db)
        res.json(films)
    }

    static filmsdetails(req: Request, res: Response): void{
        const films = db.prepare('SELECT * FROM films where id = ?').all(req.params.id)
        res.json(films)
    }

}